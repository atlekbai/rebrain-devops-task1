# rebrain-devops-task1

В данном репозитории находится дефолтный конфигурационный файл nginx

> Задание от REBRAIN

![REBRAIN logo](https://lk.rebrainme.com/img/logo_rebrain.svg)

## Нумерованные списки

В этой секции демонстрируется функционал нумерованных списков в _Markdown_

1. Элемент 1
2. Элемент 2
3. Элемент 3

## Не нумерованные списки

В этой секции демонстрируется функционал не нумерованных списков в _Markdown_

- Элемент 1
- Элемент 2
- Элемент 3

## Блоки с кодом

bash

```bash
echo "Hello, World!"
```

С++

```cpp
#include <iostream>

using namespace std;

int main(void) {
    cout << "Hello, World!" << endl;
    return 0;
}
```

## Контакты

| Соц. сеть | Ссылка                                          |
| --------- | ----------------------------------------------- |
| LinkedIN  | [Ссылка](https://www.linkedin.com/in/atlekbai/) |
| Telegram  | [Ссылка](https://t.me/atlekbai/)                |
